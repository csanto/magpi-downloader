#!/usr/bin/env python3
# AUTHOR: CSanto
# download latest MagPi if exists (exit code check)
# and send telegram notify to my account

import requests, subprocess

path="./latest_number"
latest=open(path,'r')
num = int(latest.read())
print("the latest magazine downloaded is n. "+str(num))
#print(latest.read())


while True:
    num = num+1
    link="https://www.raspberrypi.org/magpi-issues/MagPi"+str(num)+".pdf"
    print("trying to download "+link)
    # check if file exists on raspberrypi.org from http status
    r = requests.get(link)
    httpcode = r.status_code
    ##print("http code: "+ str(httpcode))
    if str(httpcode) == "200":
        print(httpcode)
        subprocess.call(['wget', str(link)])
        subprocess.call(['../telegram_bot/magazine.sh', str(num)])
    else:
     print("no more magazine found...")
     # write the latest magazine downloaded on file latest_number
     latest=open(path,'w')
     num = num-1
     ##print(num)
     latest.write(str(num)+"\n")
     break
