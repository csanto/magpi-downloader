# MagPi-Downloader

#Author: CSanto

#Description

A simple tools to download latest MagPi magazine from [raspberrypi site](https://www.raspberrypi.org/)

The script checks if the http status code is 200 to find available magazine; if it exists, script run a wget as subprocess call to download file.

Downloader needs "*latest_number*" file with the latest number you're already download.

If you want to download all the magazine overwrite the number with **1**.

After download it sends a telegram notify (**NOT included in this repo**)

